install lerna -> npm install --global lerna

ขั้นตอนการสร้าง repo  
1.สร้าง repo lerna -> git init lerna-project
2.cd lerna-project -> lerna init

การ install node แบบ install ทุกproject ใน repo
install node_module -> lerna bootstrap หรือ 
** ถ้าติด error port 42424 ใช้คำสั่งนี้แทน -> lerna bootstrap --mutex network:30330

การ install node แบบ install ไว้ข้างนอก แชร์ node ร่วมกัน
1. เพิ่ม คำสั่ง ใน lerna.json -> "useWorkspaces": true
2. เพิ่ม คำสั่ง ใน package.json (ส่วนนอกสุด) -> "workspaces": ["packages/*"]

การเพิ่มให้สามารถใช้ คำสั่งของ yarn ได้
1. เพิ่มคำสั่งใน lerna.json -> "npmClient": "yarn" 


วิธีการ run

run ทุก project -> run lerna run start 
ปล. start ต้องมีใน package.json ของแต่ละ project
run ทุก project แสดง log -> lerna run [command] --parallel
run project เฉพาะ project ที่ต้องการ -> lerna run [command] --parallel --scope=nest-start
ปล. nest-start คือชื่อของ package 
แะล [command] ตามคำสั่ง script ของ package.json  ของแต่ละโปรเจค

การเพิ่มคำสั่งใน package.json ให้รันคำสั่งสั้นลง
เพิ่ม script ให้สั้นลง ใน package.json ของ repo
"scripts": {
    "start:nest:dev": "lerna run start:dev --parallel --scope=nest-start"
}
