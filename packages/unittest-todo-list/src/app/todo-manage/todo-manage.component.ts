import { ITodoListRespone, ISuccessRespone } from './../model/todo-list';

import { Component, OnInit } from "@angular/core";
import { TodoListService } from "../services/todo-list.service";
import { FormGroup, FormControl } from '@angular/forms';
import { runInThisContext } from 'vm';

@Component({
  selector: "app-todo-manage",
  templateUrl: "./todo-manage.component.html",
  styleUrls: ["./todo-manage.component.scss"]
})
export class TodoManageComponent implements OnInit {
  todoListForm: FormGroup;
  todoList:ITodoListRespone[] = [];
  disabledAddButton: boolean = false;
  constructor(private service: TodoListService) {}

  ngOnInit(): void {

    this.todoListForm = new FormGroup ({
      topic: new FormControl(''),
      description: new FormControl('')
    })
    this.service.getList().subscribe((response: ITodoListRespone[])=>{
      this.todoList = response;
    });
  }
  add() {
    // mockdata
    // const param = { topic: "topic1", description: "desc1" }; // mock เพื่อให้ทำงานต่อไปได้
    const param = {
      topic : this.todoListForm.get('topic').value,
      description: this.todoListForm.get('description').value
    }
    this.service.create(param).subscribe((respone: ITodoListRespone) =>{

      this.todoList.push(respone);
      this.clear();
      
    },(error)=>{
      //ddddd
    });
  }
  clear(){
   // clear form
   this.todoListForm.reset();
    
  }
  selectedItem: number;
  edit(index: number){

    const itemSelected = this.todoList[index];
    this.todoListForm.get('topic').setValue(itemSelected.topic);
    this.todoListForm.get('description').setValue(itemSelected.description);
    this.disabledAddButton = true;
    this.selectedItem = index;
  }

  update(){
    // const param = {topic: 'topic1', description:'update'};

    const param = {topic: this.todoListForm.get('topic').value, description: this.todoListForm.get('description').value};

    // this.service.update(1,param).subscribe();
    this.service.update(this.todoList[this.selectedItem].id,param).subscribe((respone: ITodoListRespone) =>{
      this.todoListForm[this.selectedItem].topic = respone.topic;
      this.todoListForm[this.selectedItem].decription = respone.description;


    });
  }

  delete(index:number){
    this.service.deleteData(this.todoList[index].id).subscribe((response: ISuccessRespone)=>{
    
      this.todoList.splice(index, 1);

    })

  }
}
