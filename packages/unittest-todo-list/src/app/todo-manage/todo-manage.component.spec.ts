import { TodoListService } from "./../services/todo-list.service";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { TodoManageComponent } from "./todo-manage.component";
import { of, throwError } from "rxjs";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { expressionType } from "@angular/compiler/src/output/output_ast";
import { compileNgModule } from "@angular/compiler";
//f run เฉพาะของเรา f = focus
fdescribe("TodoManageComponent", () => {
  let component: TodoManageComponent;
  let fixture: ComponentFixture<TodoManageComponent>;
  let service: TodoListService; //1

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TodoManageComponent],
      imports: [HttpClientTestingModule], //
      providers: [TodoListService] //2,
      //
    }).compileComponents();
    service = TestBed.inject(TodoListService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoManageComponent);// เพิ่อจะสร้างตัว component เทส element ต่างๆในหน้าจอ
    component = fixture.componentInstance;
    fixture.detectChanges(); //  จำลอง เขียน component ไป binding ใน html
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
  describe("Add", () => {
    it("should call service when click and button ", () => {
      //  ถ้ามีการกดปุ่นแอดจะมีการเรียกmethod
      // expect(service.create).toHaveBeenCalled();
      // method ws create จะ responde กลับมาแล้วจะทำยังไงต่อ
      // spyOn(service, 'create');
      spyOn(service, "create").and.returnValue(of(null));

      component.todoListForm.get("topic").setValue("topic1");
      component.todoListForm.get("description").setValue("desc1");
      // เรียกใช้ function ใน component
      component.add(); //2
      // เรียก ws ภายนอกต้องทำการ mock โดยใ้ข้ spyon
      expect(service.create).toHaveBeenCalledWith({
        topic: "topic1",
        description: "desc1"
      }); //check ถึงระดัย parameter 1
    });
    // เอาค่าจาก form มาส่งให้ ws

    it("should add item to todoLIst when click and button ", () => {
      component.todoList = [
        {
          id: 1,
          topic: "topic1",
          description: "desc1"
        }
      ];
      spyOn(service, "create").and.returnValue(
        of({ id: 2, topic: "topic2", description: "desc2" })
      );
      component.todoListForm.get("topic").setValue("topic2");
      component.todoListForm.get("description").setValue("desc2");

      component.add();
      expect(component.todoList).toEqual([
        { id: 1, topic: "topic1", description: "desc1" },
        { id: 2, topic: "topic2", description: "desc2" }
      ]);
    });

    // it('should pass parameter when click and button',() =>{

    //   component.add();
    //   // คาดหวัง

    //   expect(service.create).toHaveBeenCalledWith( {id: 2, topic: 'topic2', description: 'desc2'});

    // });

    it("should clear data after click add button", () => {
      spyOn(service, "create").and.returnValue(
        of({ id: 2, topic: "topic2", description: "desc2" })
      );

      component.todoListForm.get("topic").setValue("topic2");
      component.todoListForm.get("description").setValue("desc2");

      component.add();

      expect(component.todoListForm.get("topic").value).toBeNull();
      expect(component.todoListForm.get("description").value).toBeNull();
    });
  });

  describe("Edit", () => {
    it("should set data to form after click edit button", () => {
      component.todoList = [{ id: 1, topic: "topic1", description: "desc1" }];
      expect(component.todoListForm.get("topic").value).toEqual("");
      expect(component.todoListForm.get("description").value).toEqual("");
      component.edit(0);
      expect(component.todoListForm.get("topic").value).toEqual("topic1");
      expect(component.todoListForm.get("description").value).toEqual("desc1");
    });

    it("should show edit button when click edit button ", () => {
      component.todoList = [{ id: 1, topic: "topic1", description: "desc1" }];
      component.disabledAddButton = false;
      component.edit(0);
      expect(component.disabledAddButton).toBe(true);
      // expect(component.disabledAddButton).toBeTruthy();
      //
    });
  });

  describe("Update ", () => {
    it("should call API UPDATE with new item when click update button ", () => {
      //3
      spyOn(service, "update").and.returnValues(of());
      //fill in
      component.todoListForm.get("topic").setValue("toTopicUpdate");
      component.todoListForm.get("description").setValue("toDescriptionUpdate");
      // 4
      component.selectedItem = 0;
      component.todoList = [{ id: 1, topic: "topic1", description: "desc1" }];

      //2
      component.update();
      //1
      expect(service.update).toHaveBeenCalledWith(1, {
        topic: "toTopicUpdate",
        description: "toDescriptionUpdate"
      });
    });

    it("should select item  for update when click edit button", () => {
      component.todoList = [{ id: 1, topic: "topic1", description: "desc1" }];
      //2
      component.edit(0);
      //1
      expect(component.selectedItem).toEqual(0);
    });

    it("should  update value to todoList success", () => {
      component.todoList = [{ id: 1, topic: "topic1", description: "desc1" }];
      //fill in
      component.todoListForm.get("topic").setValue("toTopicUpdate");
      component.todoListForm.get("description").setValue("toDescriptionUpdate");

      component.todoList = [
        { id: 1, topic: "toTopicUpdate", description: "toDescriptionUpdate" }
      ];
      component.selectedItem = 0;
      component.update();
      expect(component.todoList).toEqual([
        { id: 1, topic: "toTopicUpdate", description: "toDescriptionUpdate" }
      ]);
    });
  });
  describe("GET ", () => {
    it("should call service .getList when load page", () => {
      spyOn(service, "getList").and.returnValue(of([])); //controll การทำงานของ sw observale ใข้ of
      // spyOn(service, "getList").and.returnValue(of([])); // example case fail
      component.ngOnInit();
      expect(service.getList).toHaveBeenCalledTimes(1); //?
    });
    it("should call service .getList when load page error", () => {
      spyOn(service, "getList").and.returnValue(
        throwError({ code: 500, message: "xxxx" })
      ); // example case fail
    });
    it("should add  to item todoList when call service.getList service", () => {
      //3
      spyOn(service, "getList").and.returnValue(
        of([
          {
            id: 1,
            topic: "topic1",
            description: "desc1"
          },
          {
            id: 2,
            topic: "topic2",
            description: "desc2"
          }
        ])
      );
      //2
      component.ngOnInit();

      //1
      expect(component.todoList).toEqual([
        {
          id: 1,
          topic: "topic1",
          description: "desc1"
        },
        {
          id: 2,
          topic: "topic2",
          description: "desc2"
        }
      ]);
    });

    // test fail mock service ให้ thow ข้อมูลออกมา
  });

  describe("Delete ", () => {
    it("should call service.delete when click delete button", () => {
      //3
      component.todoList = [{ id: 1, topic: "topic1", description: "desc1" }];
      //2
      spyOn(service, "deleteData").and.returnValue(of());

      //1
      component.delete(0);

      //3
      expect(service.deleteData).toHaveBeenCalledWith(1);
    });

    it("should remove item form todoList when delete success", () => {
      //2
      component.todoList = [{ id: 1, topic: "topic1", description: "desc1" },
      { id: 2, topic: "topic2", description: "desc2" }];
      // 3
      spyOn(service, 'deleteData').and.returnValue(of({success: true}));
   
      //1
      component.delete(1);
    });
  });
});
