export class TodoList {
}

export interface ITodoListParam {
    topic: string;
    description: string;
    
}

export interface ITodoListRespone {
    id:number;
    topic: string;
    description: string;
}

export interface ISuccessRespone {
    success: boolean;
}

