import { ITodoListRespone, ISuccessRespone } from './../model/todo-list';
import { TestBed } from '@angular/core/testing';

import { TodoListService } from './todo-list.service';

import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing'
describe('TodoListService', () => {
  let service: TodoListService;
//set up http client 
 let httpClientTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(TodoListService);
    httpClientTestingController = TestBed.inject(HttpTestingController);
  });

  it('1.should be created', () => {
    expect(service).toBeTruthy();
  });

  //2 test create 
  describe('Create', () => {
    let param;
    //run ทุก  it
    beforeEach(()=>{
      param = {topic: 'topic1', description: 'desc1'};
    });
    //run แต่ละ test case  เสร็จ
    afterEach(()=>{
      httpClientTestingController.verify();
    })

    it('2. should call http POST when call API Create',() =>{

      // const param = {topic: 'topic1', description: 'dest1'};
      service.create(param).subscribe(()=>{

      });

      // 
      const req = httpClientTestingController.expectOne('http://localhost:3000/todos');

      // เพื่อให้ api call จริง commit ว่า error หรือ success [] คือไม่มี respone สามารถ mock respone ตรงนี้ได้
      req.flush([]);
      // ตัด req หลังจบ  ตัว verifyอาจจะมี respone ที่ค้างอยู่  
      // httpClientTestingController.verify();
      
      expect(req.request.method).toEqual('POST');
    })


     // test body เป็น json  AAA
     it('3.should pass parameter to request call API create ', () =>{
      // const param = {topic: 'topic1', description: 'desc1'};
      service.create(param).subscribe();
      const req = httpClientTestingController.expectOne('http://localhost:3000/todos');
      req.flush([]);// ไม่ต้องการ response
      expect(req.request.body).toEqual({topic: 'topic1', description: 'desc1'});
      // httpClientTestingController.verify();
     });

     it('4.should return response data when call API create success', ()=>{
      
      // const param = {topic: 'topic1', description: 'desc1'};
      service.create(param).subscribe((responseData: ITodoListRespone)=>{
        expect(responseData).toEqual({id: 1, topic: 'topic1', description: 'desc1'});
      });
      const req = httpClientTestingController.expectOne('http://localhost:3000/todos');
      req.flush({id: 1 , topic: 'topic1', description: 'desc1'});
      // httpClientTestingController.verify();
     })
     //refector code


     describe('UPDATE', () => {

      let param;
      let id: number;
      //run ทุก  it
      beforeEach(()=>{
        param = {topic: 'topic1', description: 'desc1'};
        id = 1;
      });
      //run แต่ละ test case  เสร็จ
      afterEach(()=>{
        httpClientTestingController.verify();
      })
  
    
      it('5.should call http PUSH when call API Update',() =>{
       //return body 
      //  let param = {topic: 'topic1', description: 'desc1'}
      //  let id = 1;
       service.update(id, param).subscribe(()=>{
       });
       const req = httpClientTestingController.expectOne(`http://localhost:3000/todos/${id}`);
      // เพื่อให้ api call จริง commit ว่า error หรือ success [] คือไม่มี respone สามารถ mock respone ตรงนี้ได้
      req.flush([]);
      // ตัด req หลังจบ  ตัว verifyอาจจะมี respone ที่ค้างอยู่  
      // httpClientTestingController.verify();
      expect(req.request.method).toEqual('PUT');
      });

        // test body เป็น json  AAA
     it('6.should pass parameter to request call API UPDATE ', () =>{
      // const param = {topic: 'topic1', description: 'desc1'};
      // let param = {topic: 'topic1', description: 'desc1'}
      // let id = 1;
      service.update(id, param).subscribe();
      const req = httpClientTestingController.expectOne(`http://localhost:3000/todos/${id}`);
     
      req.flush([]);// ไม่ต้องการ response
      expect(req.request.body).toEqual({topic: 'topic1', description: 'desc1'});
      // httpClientTestingController.verify();
     });

     it('should return response data when call API Update success', ()=>{
      // let id = 1;
      // let param = {topic: 'topic1', description: 'desc1'}
      // const param = {topic: 'topic1', description: 'desc1'};
      service.update(id, param).subscribe((responseData: ITodoListRespone)=>{
        expect(responseData).toEqual({id: 1, topic: 'topic1', description: 'desc1'});
      });
      const req = httpClientTestingController.expectOne(`http://localhost:3000/todos/${id}`);
      req.flush({id: 1 , topic: 'topic1', description: 'desc1'});
      //  httpClientTestingController.verify();
     })
     });




     
    })
    
    describe('GET', () => {

     it('shoult call http GET when call API GETLIST', ()=>{
           service.getList().subscribe(()=>{
      });
      const req = httpClientTestingController.expectOne('http://localhost:3000/todos');
    
      expect(req.request.method).toEqual('GET');
      req.flush([]);
      httpClientTestingController.verify();
     })

     it('should return response data when call API GET', ()=>{

      service.getList().subscribe((responseData: ITodoListRespone[])=>{
        expect(responseData).toEqual([{id:1,topic: 'aa',description: 'desc1'},{id:1,topic: 'aa',description: 'desc1'}]);
      
      });
      const req = httpClientTestingController.expectOne('http://localhost:3000/todos');
      req.flush([{id:1,topic: 'aa',description: 'desc1'},{id:1,topic: 'aa',description: 'desc1'}]);

     });

    });

    describe('DELETE', () => {

      it('shoult call http DELETE when call API deleteData', ()=>{
        let id: number = 1;
        service.deleteData(id).subscribe();
        const req = httpClientTestingController.expectOne(`http://localhost:3000/todos/${id}`);
        expect(req.request.method).toEqual('DELETE');
        req.flush([]);
        httpClientTestingController.verify();

    });
    
    it('should return response data when call API DELETE', ()=>{
      let id: number = 1;
      service.deleteData(id).subscribe((responeData: ISuccessRespone) =>{
        expect(responeData).toEqual({'success': true});
      });
       const req = httpClientTestingController.expectOne(`http://localhost:3000/todos/${id}`);
       req.flush({'success': true});

     });

  });
});
