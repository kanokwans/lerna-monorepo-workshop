import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ITodoListParam, ITodoListRespone, ISuccessRespone } from '../model/todo-list';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoListService {

  constructor(private httpClient: HttpClient) { 
    
  }

  create(param: ITodoListParam): Observable<ITodoListRespone>{
  return this.httpClient.post<ITodoListRespone>('http://localhost:3000/todos',param)
  }
  //method push update ส่ง body กับ id
  // http://localhost:3000/todos/id,{topic:'', desciption: ''}

  update(id: number, param: ITodoListParam): Observable<ITodoListRespone>{
    return this.httpClient.put<ITodoListRespone>(`http://localhost:3000/todos/${id}`,param);

  }
  //method GET
  // return [{id:1,topic: 'aa',description: 'desc1'},{id:1,topic: 'aa',description: 'desc1'}]
  getList(): Observable<ITodoListRespone[]>{
   
    return this.httpClient.get<ITodoListRespone[]>('http://localhost:3000/todos');

  }
  deleteData(id: number): Observable<ISuccessRespone>{
    return this.httpClient.delete<ISuccessRespone>(`http://localhost:3000/todos/${id}`);

  }
}
