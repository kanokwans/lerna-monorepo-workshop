import { UpdateArticleInput } from './update-article.input';
import { CreateArticleInput } from './create-article.input';
import { ArticlesService } from './articles.service';
import { Controller, Get, Param, Post, Body, Patch, Delete } from '@nestjs/common';
//localhost:3000/articles/:id
@Controller('articles')
export class ArticlesController {

    constructor(private articleService: ArticlesService ){}
    
    @Get()
    findAll(){
        return this.articleService.findAll();
    }
    
    @Get(':id')
    findOne(@Param('id') id):object{
        return this.articleService.findOne(id)
    }

    @Post()
    create(@Body() body : CreateArticleInput){
        return this.articleService.create(body);
    }

    @Patch(':id')
    update(@Param('id') id , @Body() body : UpdateArticleInput){
        return this.articleService.update(id,body)
    }

    @Delete(':id')
    delete(@Param('id') id ){
        return this.articleService.delete(id)
    }
}
