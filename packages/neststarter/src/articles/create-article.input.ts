import { ApiProperty } from '@nestjs/swagger';
export class CreateArticleInput{
    @ApiProperty()
    title:string;
    @ApiProperty()
    body :string;
}