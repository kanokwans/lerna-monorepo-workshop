import { ArticlesService } from './articles.service';
import { ArticlesController } from './articles.controller';
import { Articles } from './article.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';

@Module({
    imports : [TypeOrmModule.forFeature([Articles])],
    controllers : [ArticlesController],
    providers : [ArticlesService]
})
export class ArticlesModule {}
