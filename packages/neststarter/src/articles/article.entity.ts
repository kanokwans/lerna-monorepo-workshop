import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn} from "typeorm";

@Entity()
export class Articles {
    @PrimaryGeneratedColumn()
    id :number;

    @Column({length:110})
    title :string;

    @Column()
    body :string;

    @UpdateDateColumn()
    updateDate : Date;

    @CreateDateColumn()
    createDate : Date;

}
