import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Comments } from './comment.entity';
import { Repository } from 'typeorm';
import { CreateCommentInput } from './create-comment.input';

@Injectable()
export class CommentsService {
    
    constructor(
        @InjectRepository(Comments)
        private commentRepository : Repository<Comments>
    ){}
    
    findAll(){
        return this.commentRepository.find();
    }
    findOne(id){
        return this.commentRepository.findOne(id)
    }

    create(body : CreateCommentInput){
       const str  = this.commentRepository.create(body);
       return this.commentRepository.save(str);
    }

    async update(id,body){
        let comment = await this.commentRepository.findOne(id)
        comment = this.commentRepository.merge(comment,body)
        return this.commentRepository.save(comment)
    }
    delete(id){
        return this.commentRepository.delete(id)
    }
}
