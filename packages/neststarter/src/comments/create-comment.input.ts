import { ApiProperty } from '@nestjs/swagger';
export class CreateCommentInput{
    @ApiProperty()
    detail:string;
}