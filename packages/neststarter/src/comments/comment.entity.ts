import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn} from "typeorm";

@Entity()
export class Comments {
    @PrimaryGeneratedColumn()
    id :number;

    @Column()
    detail :string;

    @UpdateDateColumn()
    updateDate : Date;

    @CreateDateColumn()
    createDate : Date;

}

