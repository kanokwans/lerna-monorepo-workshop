import { CreateCommentInput } from './create-comment.input';
import { Controller, Get, Param, Post, Body, Patch, Delete } from '@nestjs/common';
import {CommentsService} from './comments.service'
import { UpdateCommentInput } from './update-comment.input';

@Controller('comments')
export class CommentsController {
    constructor(private commentsService: CommentsService) {
        
    }

    @Get()
    findAll(){
        return this.commentsService.findAll();
    }
    
    @Get(':id')
    findOne(@Param('id') id):object{
        return this.commentsService.findOne(id)
    }

    @Post()
    create(@Body() body : CreateCommentInput){
        return this.commentsService.create(body);
    }

    @Patch(':id')
    update(@Param('id') id , @Body() body : UpdateCommentInput){
        return this.commentsService.update(id,body)
    }

    @Delete(':id')
    delete(@Param('id') id ){
        return this.commentsService.delete(id)
    }

}
